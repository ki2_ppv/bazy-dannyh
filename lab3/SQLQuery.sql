BACKUP DATABASE Books
TO DISK = 'D:\BD\Books.bak'
WITH INIT, NAME = 'books Full Db backup',
DESCRIPTION = 'books Full Database Backup'

RESTORE DATABASE Books
FROM DISK = 'D:\BD\Books.bak'
WITH RECOVERY, REPLACE

BACKUP DATABASE Books
TO DISK = 'D:\BD\Books_FullDbBkup.bak'
 WITH INIT, NAME = 'books Full Db backup',
DESCRIPTION = 'books Full Database Backup'

BACKUP LOG Books
TO DISK = 'D:\BD\Books_FullDbBkup.bak'
WITH NOINIT, NAME = 'books Translog backup',
DESCRIPTION = 'books Transaction Log Backup', NOFORMAT


BACKUP LOG Books
TO DISK = 'D:\BD\Books_TaillogBkup.bak'
WITH NORECOVERY

RESTORE DATABASE Books
FROM DISK = 'D:\BD\Books_FullDbBkup.bak'
WITH NORECOVERY

RESTORE LOG Books
FROM DISK = 'D:\BD\Books_TlogBkup.bak'
WITH NORECOVERY

RESTORE LOG Books
FROM DISK = 'D:\BD\Books_TaillogBkup.bak'
WITH RECOVERY

BACKUP DATABASE Books
TO DISK = 'D:\BD\Books_DiffDbBkup.bak'
WITH INIT, DIFFERENTIAL, NAME = 'books Diff Db backup',
DESCRIPTION = 'books Differential Database Backup'

RESTORE DATABASE AdventureWorks
FROM DISK = 'D:\BD\Books_DiffDbBkup.bak' WITH NORECOVERY




