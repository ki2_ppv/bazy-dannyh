create login login1 with password = '1111'
use Books
create user login1 for login login1

--���� ��
alter role db_backupoperator add member login1
exec sp_helprolemember

--����� ���� ��
create role role1 authorization db_ddladmin
alter role db_ddladmin add member login1
alter role role1 add member login1
alter role db_ddladmin drop member login1

--���������� ��� ����
grant select to role1

--���������� ��� ������������
grant select to login1
grant select to login1
revoke insert to login1
deny delete to login1


USE Books
GO
CREATE SERVER ROLE [BulkAdmin2] AUTHORIZATION [sa]
GO
ALTER SERVER ROLE [BulkAdmin2] ADD MEMBER logi1
GO
GRANT Administer Bulk Operations TO [SugonyakBulkAdmin]
GO


SELECT DB_NAME() AS 'Database', p.name, p.type_desc, p.is_fixed_role, dbp.state_desc,
dbp.permission_name, so.name, so.type_desc
FROM sys.database_permissions dbp
LEFT JOIN sys.objects so ON dbp.major_id = so.object_id
LEFT JOIN sys.database_principals p ON dbp.grantee_principal_id = p.principal_id
--WHERE p.name = 'ProdDataEntry'
ORDER BY so.name, dbp.permission_name;


--���� �� �������:
exec sp_helpsrvrolemember
--���� � � ��
exec sp_helpsrvrolemember



exec sp_addlogin @loginame = 'test2_login', @passwd='2'
exec sp_adduser 'test2_login', 'test2_loginu'
grant select ON tarif to test2_loginu
--�������� ����
exec sp_addrole df
grant create table to df
--��������
exec sp_helpuser